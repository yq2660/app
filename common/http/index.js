import https from './interface'

/**
 * 将所有接口统一起来便于维护
 * 如果项目很大可以将 url 独立成文件，接口分成不同的模块
 * handle [boolean] 如果需要自己处理 catch 请求 ，传入 true ，交给接口统一处理 ，请传如 false或不传
 */

// 单独导出
export const banner = (data) => {
    return https({
        url: '/index',
        // method: 'GET', // 默认POST
        data,
		header: {
			'content-type':'11'
		}
		// handle:true
    })
}
export const login = (data) =>{
	return https({
		url : '/login',
		data,
		header:{
			'Content-Type':"application/json"
		}
	})
}
export const token = (data) =>{
	return https({
		url : '/login/token',
		data,
		header:{
			'Content-Type':"application/json"
		}
	})
}
// 默认全部导出
export default {
    banner,
	login,
	token
}