import Vue from 'vue'
import App from './App'
import Http from './common/http'
import md5 from './common/http/md5.js'
Vue.config.productionTip = false

App.mpType = 'app'
Vue.prototype.$http = Http
Vue.prototype.md5 = md5
const app = new Vue({
    ...App
})
app.$mount()
